Gravity：电化学酒精传感器（0-5ppm）

这是一款Arduino兼容的酒精浓度传感器模组，测量范围0-5ppm，支持I2C和UART两种输出方式，经过出厂标定，可以快速、准确的测量环境中酒精蒸汽的浓度。

![](./arduinoC/_images/featured.png)

# 产品简介

- 产品链接：https://www.dfrobot.com.cn/goods-2995.html

- 介绍：本扩展库为 DFRobot Gravity：电化学酒精传感器（0-5ppm)(SEN0376)设计，支持 Mind+ 导入库，要求 Mind+ 软件版本为 1.6.2 及以上。


# 积木

![](./arduinoC/_images/blocks.png)

 
 
示例程序

![](./arduinoC/_images/example.png)

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|-----|-----|
|uno||√|||
|micro:bit||√|||
|mpython||√|||
|arduinonano||√|||
|leonardo||√|||
|mega2560||√|||


# 更新日志

V0.0.1 基础功能完成

