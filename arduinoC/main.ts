//% color="#B8860B" iconWidth=50 iconHeight=40
namespace DFRobot_Alcohol{

    //% block="DFRobot_Alcohol Address [ADDRESS] MODE [MODE]" blockType="command" 
    
    //% ADDRESS.shadow="dropdown" ADDRESS.options="ADDRESS"
     //% MODE.shadow="dropdown" MODE.options="MODE"
    export function DFRobot_AlcoholInit(parameter: any, block: any) {
    
        let address = parameter.ADDRESS.code;
        let mode = parameter.MODE.code;

        Generator.addInclude("DFRobot_AlcoholInit", "#include <DFRobot_Alcohol.h>");
        
 

        Generator.addObject("DFRobot_AlcoholInit2","DFRobot_Alcohol_I2C",`Alcohol(&Wire ,${address});`);
  
        Generator.addSetup(`DFRobot_AlcoholInit`, `while(!Alcohol.begin());`);
   
        Generator.addSetup(`DFRobot_AlcoholInit1`, `Alcohol.SetModes(${mode});`);
        
        
    }
  
    //% block="CCS811  number [NUM]  " blockType="reporter" 
    //% NUM.shadow="number" NUM.defl="5"
    export function DFRobot_AlcoholRead(parameter: any, block: any) { 
        let num=parameter.NUM.code;
        
        Generator.addCode( [`Alcohol.ReadAlcoholData(${num})`,Generator.ORDER_UNARY_POSTFIX]);
 
   }
     
   
}